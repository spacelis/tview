# TweetView

This is a small tool for exploring text tweets.


## Install

```
git clone https://bitbucket.org/spacelis/tview.git
cd tview
```

Installation is just one command as below which only installs some required 
library for running the application.


### Requirement Setup
If you have global write access to the python installation, proceed with
```
pip install -r requirements.txt
```

Local installation can be done as follows:
```
export PYTHONUSERBASE=$HOME/myappenv
pip install -r requirements.txt --user
```

A virtualenv package is also included in *lib* directiory. You may simply run
following commmands to setup a virtualenv for running the application.

```
bin/setup.sh
```

After this a directory called *py27* will show up containing all
needed configuration for a virtualenv. Now you just need to enter the
virtualenv for running the actual application. To enter the virtualenv:

```
py27/bin/activate
```

To leave from virtualenv:

```
deactivate
```


## How to use

To actually use this application, there must be some data to explore and they
should be put in the directory *data*.

Next, run the command

```
python application.py
```

Then open a browser with address 

```
http://localhost:5000/<the filename of the date file>.
```

On the page there four parts. The top one is an input box for queries,
while the second top part is a hourly distribution of tweets matching
the query. Below that is a word cloud showing the words occured in the
tweets. The lowest part is the list of matching tweets.

To stop the application, press <ctrl-c>.


### For Virtualenv setup

If you setup the application by using virtualenv as described in
previous section. You may also run the application with single command
as follows:

```
bin/run.sh
```
