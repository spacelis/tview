#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" An interface viewing html scraped tweets.

File: application.py
Author: SpaceLis
Email: Wen.Li@tudelft.nl
GitHub: http://github.com/spacelis
Description:
    Requires: Flask, Werkzeug, jinjia2, whoosh, python-dateutil

"""
import os
import sys
if 'win32' in sys.platform:
    import win32com.shell.shell as shell
    ASADMIN = 'asadmin'
    if sys.argv[-1] != ASADMIN:
        script = os.path.abspath(sys.argv[0])
        params = ' '.join([script] + sys.argv[1:] + [ASADMIN])
        shell.ShellExecuteEx(lpVerb='runas', lpFile=sys.executable,
                             lpParameters=params)
        sys.exit(0)

import json
from dateutil.parser import parse as parsedate

import whoosh.index as widx
import whoosh.fields as wf
from whoosh.qparser import QueryParser

from flask import render_template
from flask import jsonify
from flask import Flask

SCHEMA = wf.Schema(id=wf.STORED,
                   text=wf.TEXT(stored=True),
                   created_at=wf.DATETIME(stored=True),
                   screen_name=wf.KEYWORD(stored=True),
                   user_id=wf.STORED)
IDXDIR = 'idx'
DATADIR = 'data'

app = Flask(__name__, static_folder='static', static_url_path='/static')


@app.route("/")
def listdir():
    """ List files in the data dir.
    :returns: @todo

    """
    return render_template(
        'datafiles.html',
        fs=[('/' + n, n) for n in os.listdir('data')
            if n != 'README'])


@app.route("/<filename>")
def browse(filename):
    """ The main page. """
    indexed = widx.exists_in(IDXDIR, filename)
    return render_template('main.html',
                           filename=filename,
                           indexed=str(indexed).lower())


@app.route("/api/process/<filename>")
def process(filename):
    """ The api for index tweets. """
    path = os.path.join(DATADIR, filename)
    if not os.path.isfile(path):
        return jsonify(succeeded=False)
    ix = widx.create_in('idx', SCHEMA, indexname=filename)
    ixwriter = ix.writer()
    with open(path) as fin:
        for line in fin:
            d = json.loads(line)
            ixwriter.add_document(id=d['id'],
                                  text=d['text'],
                                  created_at=parsedate(d['created_at']),
                                  screen_name=d['user']['screen_name'],
                                  user_id=d['user']['id'])
        ixwriter.commit()
    return jsonify(succeeded=True)


@app.route('/api/search/<filename>/<query>')
def search(filename, query):
    """ Search the query. """
    ix = widx.open_dir(IDXDIR, schema=SCHEMA, indexname=filename)
    with ix.searcher() as searcher:
        query = QueryParser("text", ix.schema).parse(query)
        results = searcher.search(query, limit=None)
        return jsonify(tweets=[{'id': r['id'],
                                'text': r['text'],
                                'screen_name': r['screen_name'],
                                'created_at': r['created_at'].ctime()}
                               for r in results],
                       succeeded=True)


# -- mocking --
@app.route("/api/mock_data")
def mockdata():
    """ A mockdata api for testing. """
    import random
    data = [
        {'text': 'first tweet' + str(random.random())},
        {'text': 'second tweet' + str(random.random())}
    ]
    return jsonify(tweets=data, succeeded=True)


if __name__ == "__main__":
    app.debug = True
    app.run()
