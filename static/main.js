/* global dc */
/* global d3 */
/* global crossfilter */
/* global WordFreq */
/* global WordCloud */

var tweetcharts = (function(){

  var data;
  var search_api_url = "/api/search/";
  var time_parser = function(d){return new Date(d);};
  var time_format = d3.time.format("%Y-%m-%d %H:%M");

  function search(query, filename){
    d3.json(search_api_url + filename + '/' + query, function(err, json){
      if(!err && json.succeeded){
        update(json.tweets);
      } else {
        alert("An error occurred during downloading data.");
      }
    });
  }

  function render(){
    var fact = crossfilter(data);

    var by_id = fact.dimension(function(c){return c.id;});

    var by_hour = fact.dimension(function(c){
      return d3.time.hour(c.created_at);
    });
    var checkins_by_hour = by_hour.group().reduceCount();

    var timeline_chart = dc.barChart("#chart-timeline")
      .width(screen.availWidth) // (optional) define chart width, :default = 200
      .height(120) // (optional) define chart height, :default = 200
      .transitionDuration(500) // (optional) define chart transition duration, :default = 500
      .dimension(by_hour) // set dimension
      .group(checkins_by_hour) // set group
      .elasticX(false)
      .x(d3.time.scale().domain([new Date("2013-10-10"), new Date("2013-10-20")]))
      //.round(d3.time.hour.round)
      .xUnits(d3.time.hours)
      .elasticY(true)
      //.y(d3.scale.linear().domain([0, 20]))
      .centerBar(true)
      .gap(1)
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .brushOn(true)
      .title(function(d) { return "Value: " + d.value; })
      //.on("filtered", function(chart, filter){
        //update_map(checkins_by_poi);
      //})
      .renderTitle(true);

    var links = /((http(s)?:\/\/)?\w+\.\w+((\/)?[\w#!:.?+=&%@!\-\/]+)?)/;
    var tweet_table = dc.dataTable("#tweet-table")
      .dimension(by_id)
      .group(function (d) {
        return d.id;
      })
      .columns([
        function (d) {
          return '<a target="_blank" href="https://twitter.com/' + d.screen_name +
            '/status/' + d.id + '">' + d.id + '</a>';
        },
        function (d) {
          return time_format(d.created_at);
        },
        function (d) {
          return '<a target="_blank" href="https://twitter.com/' + d.screen_name + '">' + d.screen_name + '</a>';
        },
        function (d) {
          return d.text.replace(links, '<a target="_blank" href="$1"> $1 </a>');
        },
      ]);
    dc.renderAll();

    // ----       Word Cloud
    // From Jonathan Feinberg's cue.language, see lib/cue.language/license.txt.

    function wf_preprocess(ds) {
      if (ds.length > 1000) {
        var sample_rate = 1000 / ds.length;
        return ds.map(
          function(d){
            if(Math.random() <= sample_rate)
              return d.text.toLowerCase().replace(links, " ").replace("http", " ");
          });
      }
      return ds.map(
        function(d){return d.text.toLowerCase().replace(links, " ").replace("http", " ");});
    }
    var wordfreq_opt = {
      workerUrl: '/static/wordfreq.worker.js',
      stopWords: ['english1', 'english2', 'dutch'],
      minimumCount: 3,
    };
    
    var dutchStopWords = [
      'aan', 'af', 'al', 'alles', 'als', 'altijd', 'andere', 'ben', 'bij',
      'daar', 'dan', 'dat', 'de', 'der', 'deze', 'die', 'dit', 'doch', 'doen',
      'door', 'dus', 'een', 'eens', 'en', 'er', 'ge', 'geen', 'geweest',
      'haar', 'had', 'heb', 'hebben', 'heeft', 'hem', 'het', 'hier', 'hij',
      'hoe', 'hun', 'iemand', 'iets', 'ik', 'in', 'is', 'ja', 'je', 'kan',
      'kon', 'kunnen', 'maar', 'me', 'meer', 'men', 'met', 'mij', 'mijn', 'moet',
      'na', 'naar', 'niet', 'niets', 'nog', 'nu', 'of', 'om', 'omdat', 'ons',
      'ook', 'op', 'over', 'reeds', 'te', 'tegen', 'toch', 'toen', 'tot',
      'u', 'uit', 'uw', 'van', 'veel', 'voor', 'want', 'waren', 'was', 'wat',
      'we', 'wel', 'werd', 'wezen', 'wie', 'wij', 'wil', 'worden',
      'zal', 'ze', 'zei', 'zelf', 'zich', 'zij', 'zijn', 'zo',
      'zonder', 'zou',
    ];

    var wordprocessor = new WordFreq(wordfreq_opt);
    wf_preprocess(data).forEach(function(d){wordprocessor.process(d);});
    
    wordprocessor.getList(function (list) {
      build_cloud2(list.filter(function(d){return dutchStopWords.indexOf(d[0]) < 0;}));
    });

    
    function move_hlbox(x, y, w, h) {
      $("#word-left").css({
        left: x, top: y,
        height: h, width: 1,
        "margin-left": "15px",
        border: "2px dashed #000",
        display: "inline",
        position: "absolute"
      });
      $("#word-top").css({
        left: x, top: y,
        height: 1, width: w,
        "margin-left": "15px",
        border: "2px dashed #000",
        display: "inline",
        position: "absolute"
      });
      $("#word-right").css({
        left: x + w, top: y,
        height: h + 4, width: 1,
        "margin-left": "15px",
        border: "2px dashed #000",
        display: "inline",
        position: "absolute"
      });
      $("#word-bottom").css({
        left: x, top: y + h,
        height: 1, width: w + 4,
        "margin-left": "15px",
        border: "2px dashed #000",
        display: "inline",
        position: "absolute"
      });
    }

    function hide_hlbox(){
      $(".wordbox").css({display: "none"});
    }
    $("canvas").mouseout(hide_hlbox);

    function build_cloud2(list){
      WordCloud(document.getElementById('word-cloud'),
        { list: list,
          hover: function(item, dimension, event){
            if(dimension){
              move_hlbox(dimension.x, dimension.y, dimension.w, dimension.h);
            } else {
              hide_hlbox();
            }
          },
        click: function(item, dimension, event){
          var e = jQuery.Event("keydown");
          e.keyCode = 13;
          $("#q").val(item[0]).trigger(e);
        }
        });
    }
  }

  function update(payload){
    payload.forEach(function (c){
      c.created_at = time_parser(c.created_at);
    });
    data = payload;
    render();
  }

  function init(){
  }

  return {
    search: search,
    update: update,
    init: init,
  };
}());
